from rest_framework.permissions import BasePermission
from rest_framework.exceptions import APIException
from rest_framework import status

from ASM_Backend.utils import message
from accounts import models as accounts_model


class GroupPermission(BasePermission):
    def has_permission(self, request, view):
        view_name = view.__class__.__name__
        if request.user.is_authenticated:
            accesspage_queryset = accounts_model.AccessPage.objects.filter(view_names__contains=[view_name])
            if accesspage_queryset:
                page_id = accesspage_queryset[0].page_id
                user_queryset = accounts_model.User.objects.filter(email=request.user.email,page_access__contains=[page_id])
                if user_queryset:
                    return True
        raise UnauthorizeUser()

class UnauthorizeUser(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = {'status':False,
                    'message': message.UNAUTHORIZE_ACCESS,"result":{}}
    default_code = 'not_authenticated'