
# class MediaStorage(S3Boto3Storage):
#     bucket_name = AWS_STORAGE_BUCKET_NAME
#     custom_domain = '{}.s3-accelerate.amazonaws.com'.format(bucket_name)


from storages.backends.s3boto3 import S3Boto3Storage
from ASM_Backend.settings import AWS_STORAGE_BUCKET_NAME,REGION_NAME
import os
from  tempfile import SpooledTemporaryFile
import boto3

class MediaStorage(S3Boto3Storage):

    custom_domain = '{}.s3.amazonaws.com'.format(AWS_STORAGE_BUCKET_NAME)
    # print("custom_domain : ",custom_domain)
    def __init__(self, *args, **kwargs):
        super(MediaStorage, self).__init__(*args, **kwargs)
        self.client = boto3.client('s3')


    def _save(self, name, content):
        """
        We create a clone of the content file as when this is passed to
        boto3 it wrongly closes the file upon upload where as the storage
        backend expects it to still be open
        """
        # Seek our content back to the start
        content.seek(0, os.SEEK_SET)

        # Create a temporary file that will write to disk after a specified
        # size. This file will be automatically deleted when closed by
        # boto3 or after exiting the `with` statement if the boto3 is fixed
        with SpooledTemporaryFile() as content_autoclose:

            # Write our original content into our copy that will be closed by boto3
            content_autoclose.write(content.read())

            # Upload the object which will auto close the
            # content_autoclose instance
            return super(MediaStorage, self)._save(name, content_autoclose)
