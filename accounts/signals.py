from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.auth.models import Group
from django.dispatch import receiver
from django.db.models.signals import post_save,pre_save
from django.db import transaction

from accounts import models as accounts_model
from accounts.utils import send_password_generation_mail
from ASM_Backend import constant
import threading
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError

@receiver(pre_save,sender=accounts_model.User)
def user_pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.full_name:
		instance.full_name = instance.first_name + ' ' + instance.last_name
	


@receiver(post_save,sender=accounts_model.User)
def user_post_save_receiver(sender, instance, created, *args, **kwargs):
	if instance.user_group_id:
		if not instance.groups.filter(id=instance.user_group_id.id).exists():
			group = Group.objects.get(name=instance.user_group_id.name)
			transaction.on_commit(lambda: instance.groups.set([group], clear=True))
			print("UPdated Group")


	if created:
		if not instance.is_superuser:
			if instance.user_group_id:
					
				# group = Group.objects.get(name=instance.user_group_id.name)
				# instance.groups.add(group)
				# Send an Reset Password Email
				email =instance.email
				user = accounts_model.User.objects.get(email=email)
				uidb64 = urlsafe_base64_encode(smart_bytes(user.user_uid))
				token = PasswordResetTokenGenerator().make_token(user)
				t = threading.Thread(target=send_password_generation_mail,args=[user,uidb64,token,"Generate New Password"],daemon=True)
				t.start()
				# send_password_generation_mail(user,uidb64,token,"Generate New Password")
				print("Email Thread Started")
	else:
		if instance.is_locked and instance.login_counter>=constant.WRONG_PASSWORD_LIMIT:
			email =instance.email
			user = accounts_model.User.objects.get(email=email)
			uidb64 = urlsafe_base64_encode(smart_bytes(user.user_uid))
			token = PasswordResetTokenGenerator().make_token(user)
			t = threading.Thread(target=send_password_generation_mail,args=[user,uidb64,token,"Reset Password"],daemon=True)
			t.start()
			# send_password_generation_mail(user,uidb64,token,"Reset Password")
			instance.login_counter = 0
			instance.save()
			print("Email Sended For wrong password Threadsibng")


			
			# organization = Oragnization.objects.get(oragnization_id=instance.oragnization_id.oragnization_id)

			# print("Current COunt : ",organization.live_user)
			# print(instance.oragnization_id.is_max_user)
			# if not instance.oragnization_id.is_max_user:
			# 	organization.live_user += 1
			# 	if organization.live_user == organization.user_limit:
			# 		organization.is_max_user = True
			# 	organization.save()
			# 	instance.save()
			# print("Post COunt : ",organization.live_user)
			