import os
WRONG_PASSWORD_LIMIT = 3


EXPIRE_DATE_LIMIT = 30 # if This is expire date, if expire date is less 30 days make flag True

MOBILE_API_URL = os.environ.get("MOBILE_API_URL")
MOBILE_WEBSITE_URL  = os.environ.get("MOBILE_WEBSITE_URL")
MOBILE_API_AUTHORIZATION = os.environ.get("MOBILE_API_AUTHORIZATION")


TELEGRAM_API_ID = os.environ.get("TELEGRAM_API_ID")
TELEGRAM_API_HASH = os.environ.get("TELEGRAM_API_HASH")
TELEGRAM_PHONE_NUM = os.environ.get("TELEGRAM_PHONE_NUM")
TELEGRAM_USERNAME = os.environ.get("TELEGRAM_USERNAME")