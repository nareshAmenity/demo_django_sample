import re
from rest_framework import serializers
from phonenumber_field.modelfields import PhoneNumberField

from ASM_Backend.utils import message
from accounts.models import User,Oragnization,OragnizationPlan
from datetime import datetime,date


from ASM_Backend import constant
def password_validator(password):
    if re.findall('[a-z]', password):
        if re.findall('[A-Z]', password):
            if re.findall('[0-9]', password):
                if re.findall("[~\!@#\$%\^&\*\(\)_\+{}\":;'\[\]]", password):
                    if len(password) > 7:
                        return True


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['user_uid','email','username','full_name']

class UserRegisterSerializer(serializers.ModelSerializer):
        pass

class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, error_messages={'required': message.NECESSARY_EMAIL_ADDRESS, 'blank': message.NECESSARY_EMAIL_ADDRESS})
    password = serializers.CharField(required=True, error_messages={'required':message.NECESSARY_PASSWORD, 'blank': message.NECESSARY_PASSWORD})

    
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        try:
            user = User.objects.get(email=email)
        except Exception as e:
           raise serializers.ValidationError(
                'Please enter valid email address')     
                
        if user.is_locked:
            raise serializers.ValidationError("User is locked, Please check email to reset password.")

        if not user.check_password(password):
            user.login_counter += 1
            user.save()
            if user.login_counter >= constant.WRONG_PASSWORD_LIMIT:
                user.is_locked = True
                user.save()
                
            raise serializers.ValidationError('Please enter valid password')

        if not user.is_verified:
            raise serializers.ValidationError('User not verified, please verify user before logging in')

        if user.is_verified and not user.is_active:
            raise serializers.ValidationError('User is not active')

        
        user_oraganization = Oragnization.objects.get(oraganization_name=user.oragnization_id.oraganization_name)
        if not user_oraganization.is_active:
            raise serializers.ValidationError("Plan Is expired, Please contact Cycatz Team for Renewal")
        
        user_oraganization_plan = OragnizationPlan.objects.get(plan_id=user_oraganization.oragnization_plan_id.plan_id)
        purchase_date = user_oraganization.purchase_date
        today_date = date.today()
        delta = today_date - purchase_date
        if delta.days > user_oraganization_plan.plan_duration:
            user_oraganization.is_active=False
            user_oraganization.save()
            raise serializers.ValidationError("Plan Is expired, Please contact Cycatz Team for Renewal")
        
        return attrs


class ViewEditUserSerailizer(serializers.ModelSerializer):
    email = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    phone_number = PhoneNumberField()
    profile_picture = serializers.ImageField(required=False, allow_null=True)
    username = serializers.CharField(required=False)
    region = serializers.CharField(required=False)



    class Meta:
        model = User
        fields = ['email','first_name','last_name','phone_number','profile_picture','username','region']


    def validate(self, data):
        profile_picture = data.get('profile_picture')

        photo_size_bit = 1024*1024*5     # set 5MB veriable
        if profile_picture is not None and (profile_picture.size > photo_size_bit):
            raise serializers.ValidationError('Image size should be 5MB or less')
        
        return data

    def validate_username(self,username):
        if len(username) < 6 or len(username) > 15:
            raise serializers.ValidationError('Username must be between 6 and 15 characters long')
        
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError("UserName is Already Taken. Please try another.")
        return username



class ChangePasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField(required=True, error_messages={'required': 'Old password is required', 'blank': 'Old password is required'})
    new_password = serializers.CharField(required=True, error_messages={'required': 'New password is required', 'blank': 'New password is required'})
    confirm_new_password = serializers.CharField(required=True, error_messages={'required': 'Confirm password is required', 'blank': 'Confirm password is required'})

    def validate(self, attrs):
        user = self.context.get('user')
        current_password = attrs.get('current_password')
        new_password = attrs.get('new_password')
        confirm_new_password = attrs.get('confirm_new_password')

        if not user.is_active:
            raise serializers.ValidationError('User is inactive')

        if not user.check_password(current_password):
            raise serializers.ValidationError('Old Password is not correct')

        if not password_validator(new_password):
            raise serializers.ValidationError('Password must contain 1 number, 1 upper-case and lower-case letter and a special character')

        if current_password == new_password:
            raise serializers.ValidationError('New password cannot be same as old password')

        if new_password != confirm_new_password:
            raise serializers.ValidationError('The new passwords do not match')

        return attrs


class ForgotPasswordSendEmailViewSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, error_messages={'required': message.NECESSARY_EMAIL_ADDRESS, 'blank': message.NECESSARY_EMAIL_ADDRESS})

    class Meta:
        model = User
        fields = ('email',)

    def validate(self, attrs):
        email = attrs.get('email')

        try:
            user = User.objects.get(email=email)
        except Exception as e:
            raise serializers.ValidationError(
                "Email Address is invalid.")

        if not user.is_active:
            raise serializers.ValidationError('User is not active')

        return attrs

        

class ForgotPasswordVerifyViewSerializer(serializers.Serializer):
    new_password = serializers.CharField(required=True, error_messages={'required': 'New Password is required', 'blank': 'New Password is required'})
    confirm_new_password = serializers.CharField(required=True, error_messages={'required': 'Confirm Password is required', 'blank': 'Confirm Password is required'})

    def validate(self, attrs, *args, **kwargs):
        user = self.context.get('user')
        try:
            new_password = attrs.get('new_password')
            confirm_new_password = attrs.get('confirm_new_password')
        except Exception as e:
            raise serializers.ValidationError("Enter Password and Confirm Password")

        if user.check_password(new_password):
            raise serializers.ValidationError(
                'New password cannot be same as old password')

        if not password_validator(new_password):
            raise serializers.ValidationError(
                'Password must contain 1 number, 1 upper-case and lower-case letter and a special character')

        if new_password != confirm_new_password:
            raise serializers.ValidationError(
                'The new passwords do not match')

        return attrs