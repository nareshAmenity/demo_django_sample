import os

sep = os.sep

logs_folder = 'django_logs'
fileHandler = 'logging.FileHandler'
if not os.path.isdir(logs_folder):
        os.makedirs(logs_folder)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            # 'format': "{levelname} {asctime} {funcName} {lineno} {pathname} {message}"  #'{levelname} {asctime} {module} {process} {thread} {message}'
            "format": "{levelname} {asctime} {funcName} {lineno} {pathname}: {message}",
            "style": "{",
            "datefmt": "%d-%m-%Y %H:%M:%S",
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
    },
    'handlers': {
        'file_debug': {
            'level': 'DEBUG',
            'class': fileHandler,
            'filename': f'.{sep}{logs_folder}{sep}debug.log',
            'formatter': 'verbose',
        },
        'file_info': {
            'level': 'INFO',
            'class': fileHandler,
            'filename': f'.{sep}{logs_folder}{sep}info.log',
            'formatter': 'verbose',
        },
        'file_warning': {
            'level': 'WARNING',
            'class': fileHandler,
            'filename': f'.{sep}{logs_folder}{sep}warning.log',
            'formatter': 'verbose',
        },
        'file_error': {
            'level': 'ERROR',
            'class': fileHandler,
            'filename': f'.{sep}{logs_folder}{sep}error.log',
            'formatter': 'verbose',
        },
        'mail_admins': {
        'level': 'ERROR',
        'class': 'django.utils.log.AdminEmailHandler',
        'formatter': 'verbose'
        },
    },
    'loggers': {
        'drone_backend': {
            'handlers': ['file_debug', 'file_info', 'file_warning', 'file_error'],
            'level': 'DEBUG',
            'propagate': True,
        },
    'django.request': {
        'handlers': ['mail_admins'],
        'level': 'ERROR',
        'propagate': True,
        },
    },
}