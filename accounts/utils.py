from django.template.loader import render_to_string
from django.core.mail.message import EmailMultiAlternatives

from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.response import Response

from ASM_Backend import settings
import traceback

def get_tokens_for_user(user):
    """
    This method will generate token for the 
    particular user.
    Params:
       user: User model

    Returns:
        
    """
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


def send_multi_format_email(template_prefix, template_ctxt, target_email, email_subject):
    """
    This method is used to render a template and send it into the mail
    
    Params:
       template_prefix : Name of the template to load from static dir
       template_ctxt : Template render data
       target_email : Receiver's email address
       subject : Subject line of the email

    Returns:
        
    """
    try:
        email_subject, from_email, to = 'Verify your Email Address', settings.EMAIL_FROM, target_email
        bcc_email = settings.EMAIL_BCC

        txt_file = f'accounts/{template_prefix}.txt'
        text_content = render_to_string(txt_file, template_ctxt)

        html_file = f'accounts/{template_prefix}.html'
        html_content = render_to_string(html_file, template_ctxt)

        msg = EmailMultiAlternatives(email_subject, text_content, from_email, [to], bcc=[bcc_email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        print("EMail sended Succesfully")
    except Exception as e:
        traceback.print_exc()
        print(e)


def send_password_generation_mail(user,uidb64,token,mail_subject,mail_template_prefix='forgot_password'):
    """
    This method is to create a template render data from model
    
    Params:
       user : User model to load email and name
       uidb64 : Token 
       token : passwordgenerator token
       subject : Subject of email
       
    Returns:
        
    """    
    reset_url = settings.MAIL_URL+f"/reset-password/{user.user_uid}/{uidb64}/{token}/"
    template_ctxt = {
    'email': user.email,
    'first_name': user.first_name,
    'token': token,
    'user_uid': user.user_uid,
    'uidb64' : uidb64,
    'mail_url' : settings.MAIL_URL,
    'reset_url': reset_url
    }

    send_multi_format_email(mail_template_prefix, template_ctxt, user.email, mail_subject)


