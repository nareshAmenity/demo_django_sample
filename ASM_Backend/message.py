QUERY_PARAMS_NOT_FOUND = "Please check Query Params."
PAGE_NOT_FOUND = "Please check Data is not availabel for given params, Page not Found"
UNAUTHORIZE_ACCESS = "You are not authorize to access this page"


ORAGANIZATION_USER_SUCCESS = "User Details Fetched Succesfully"
ACCESSPAGE_DATA_SUCCESSFULL = "Page data Fetched Succesfully"
ALLSUBDOMAIN_DETAILS_SUCCESS = "All Subdomain Details"
DOMAIN_DETAILS_SUCCESS = "Domain Details Fetched."

ASSEST_COUNT_SUCCESS = "Fetched Assest Count Successfully"
LOOKLIKE_COUNT_DATA_SUCCESS = "Fetched Looklike table count Successfully"
SSL_COUNT_DATA_SUCCESS = "Fetched Ssl table count Successfully"

MISMATCH_DATA_FAIL = "Data Doesn't Exists for this query."