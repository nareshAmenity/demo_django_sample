from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager, PermissionsMixin,AbstractUser
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.postgres.fields import ArrayField
from django_resized import ResizedImageField


# External Library
from phonenumber_field.modelfields import PhoneNumberField
from ASM_Backend.models import BaseModel as ASM_Backend_BaseModel
from accounts.utils import send_password_generation_mail
import uuid

# from accounts import signals
# Create your models here.

alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', message='Only alphanumeric characters are allowed.')


class UserManager(BaseUserManager):
	def create_user(self, email, password=None):
		if not email:
			raise ValueError("User must have an email address")

		user = self.model(
			email = self.normalize_email(email),
		)

		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, password):
		user = self.create_user(
			email = self.normalize_email(email),
			password = password
		)

		user.is_admin   =   True
		user.is_superuser = True
		user.is_staff = True
		user.is_active = True
		user.save(using=self._db)
		return user


def user_wise_images(instance, filename):
    """ This methis is used to create valid path to save
        user's original profile picture according their ID

    Params:
        instance: created user's data
        filename: name of the file

    Returns:
        Valid: folder_path
    """
    folder_path = f'users/{instance.user_uid}/profile_picture/{filename}'
    return folder_path

def user_wise_resized_profile_pic(instance, filename):
    """ This methis is used to create valid path to save
        user's resized profile picture according their ID

    Params:
        instance: created user's data
        filename: name of the file

    Returns:
        Valid: folder_path
    """
    folder_path = f'users/{instance.user_uid}/resized_profile_picture/{filename}'
    return folder_path    

class AccessPage(ASM_Backend_BaseModel):
	page_id = models.BigAutoField(auto_created=True,primary_key=True, serialize=False, verbose_name='ID')
	page_name = models.CharField(max_length=50,unique=True)
	view_names = ArrayField(models.CharField(max_length=500),default=list)
	def __str__(self):
		return self.page_name

class User(ASM_Backend_BaseModel,AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(max_length=100, unique=True)
	username = models.CharField(max_length=20,validators=[alphanumeric],unique=True)
	user_uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	oragnization_id = models.ForeignKey("accounts.Oragnization",on_delete=models.CASCADE, related_name="oragnizations_id", db_column="oragnization_id",null=True)
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	full_name = models.CharField(max_length=100)  
	region = models.CharField(max_length=100,blank=True,null=True)
	phone_number = PhoneNumberField(null=True, blank=True)
	user_group_id = models.ForeignKey(Group,on_delete=models.CASCADE,related_name="user_permission_group",db_column="user_group_id",null=True)
	profile_picture = models.ImageField(
		upload_to=user_wise_images, null=True, blank=True)
	resized_profile_pic = ResizedImageField(size=[100, 100], quality=95, upload_to=user_wise_resized_profile_pic, null=True, blank=True)
	page_access = ArrayField(models.PositiveIntegerField(),blank=True,null=True)

	login_counter = models.PositiveIntegerField(default=0)
	
	
	is_active = models.BooleanField(default=False)
	is_admin = models.BooleanField(default=False)
	is_superuser = models.BooleanField(default=False)
	is_verified = models.BooleanField(default=False)
	is_staff = models.BooleanField(default=False)
	is_locked = models.BooleanField(default=False)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	objects = UserManager()

	class Meta: 
		verbose_name = "User"
		verbose_name_plural = "Users"

	def __str__(self):
		return self.email

	def has_perm(self, perm, obj=None):
		return self.is_admin

	def get_user_all_data(self):
		return {
			'user_uid': self.user_uid, 
			'email': self.email, 
			'username':self.username,
			'first_name': self.first_name, 
			'last_name': self.last_name,
			'full_name': self.full_name,
			'is_verified': self.is_verified,
			'is_active': self.is_active,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
			'user_group_id':self.user_group_id.name,
			"oragnization_id":self.oragnization_id.oraganization_name if self.oragnization_id else None ,
			'profile_picture' : self.profile_picture.url if self.profile_picture else None,
			'resized_profile_pic' : self.resized_profile_pic.url if self.resized_profile_pic else None,
			'phone_number': str(self.phone_number.national_number) if self.phone_number else None,
			'region': self.region,
		}
		
		
	def get_User_data(self, fields):
		user_details = self.get_user_all_data()
		if fields[0] == 'all':
			return self.get_user_all_data()
		# print(fields)
		return dict(zip(fields, map(lambda field: user_details[field], fields)))


class OragnizationPlan(ASM_Backend_BaseModel):
	plan_id = models.BigAutoField(auto_created=True,primary_key=True, serialize=False, verbose_name='ID')
	plan_name = models.CharField(max_length=20,unique=True)
	plan_duration = models.IntegerField(default=180,null=True)

	class Meta:
		verbose_name = "Oranization Plan"
		verbose_name_plural = "Oraganization Plans"

	def __str__(self) -> str:
		return self.plan_name



class Oragnization(ASM_Backend_BaseModel):
	oragnization_id = models.BigAutoField(auto_created=True,primary_key=True, serialize=False, verbose_name='ID')
	oraganization_name = models.CharField(max_length=50)
	oragnization_plan_id = models.ForeignKey("accounts.OragnizationPlan", on_delete=models.CASCADE, related_name="oragnization_plan_id", db_column="plan_id")
	live_user = models.PositiveIntegerField()
	user_limit = models.PositiveIntegerField(default=5)
	is_active = models.BooleanField(default=True)
	purchase_date = models.DateField(blank=False,null=False)

	class Meta:
		verbose_name = "Oranization"
		verbose_name_plural = "Oraganizations"

	def __str__(self) -> str:
		return self.oraganization_name
