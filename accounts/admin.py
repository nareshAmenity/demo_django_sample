from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin

from ASM_Backend import settings
from accounts.models import User,Oragnization,OragnizationPlan,AccessPage


# Register your models here.

class AcceessPageAdmin(admin.ModelAdmin):
    model = AccessPage
    list_display = ('page_id','page_name','view_names')
    readonly_fields = ('page_id', 'created_at', 'updated_at')

    def get_form(self, request, obj=None, **kwargs):
        form = super(AcceessPageAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['view_names'].widget.attrs['style'] = 'width: 70em;'
        return form

class AccountAdmin(UserAdmin):
    model = User
    list_display = ('email', 'user_uid','username', 'oragnization_id','is_verified', 'is_admin',)
    search_fields = ('email',)
    readonly_fields = ('user_uid', 'last_login', 'created_at', 'updated_at')
    fieldsets = (
        ('User Info', {'fields': ('user_uid', 'email', 'first_name', 'last_name', "full_name", 'last_login', 'created_at', 'updated_at','oragnization_id','user_group_id','page_access',
        'profile_picture','resized_profile_pic','phone_number','username','region','is_locked','login_counter')}),
        ('Email Info', {'fields': ('is_verified',)}),
        ('User Role', {'fields': ('is_admin', 'is_active', 'is_staff', 'is_superuser',)}),
        ('Permissions', {'fields': ('groups', 'user_permissions',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'first_name', 'last_name','oragnization_id','user_group_id','profile_picture',
            'resized_profile_pic','phone_number','username',"region"),
        }),
    )
    ordering = ('email',)

class OrganizationAdmin(admin.ModelAdmin):
    model = Oragnization
    list_display = ('oragnization_id', 'oraganization_name','oragnization_plan_id', 'live_user','is_active', )
    readonly_fields = ('oragnization_id','created_at', 'updated_at')

admin.site.register(User, AccountAdmin)
# admin.site.register(User)
admin.site.register(OragnizationPlan)
admin.site.register(Oragnization,OrganizationAdmin)
admin.site.register(AccessPage,AcceessPageAdmin)