from django.shortcuts import render
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import update_last_login

# Rest Framwork Import
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken, BlacklistedToken
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView


from accounts.serializers import UserLoginSerializer,ViewEditUserSerailizer,ChangePasswordSerializer,\
                                ForgotPasswordSendEmailViewSerializer,ForgotPasswordVerifyViewSerializer,UserListSerializer
from accounts.models import User
from accounts.utils import get_tokens_for_user,send_multi_format_email
from ASM_Backend import settings
from ASM_Backend.utils.customeresponse import create_response
from ASM_Backend.utils import message

from PIL import Image
import io,sys,os
import requests
import traceback
from datetime import datetime
import threading

def error404(request,exception):
    raise NotFound(detail="Error 404, page not found", code=404)


class UserLoginView(APIView):
    """ This view is used to Login

    """
    serializer_class = UserLoginSerializer

    def post(self, request, format=None):
        """ This method is used to Login

        Returns:
            Valid: User successfully logged in
            Error: Please enter valid Email address and Password
        """
        serializer = self.serializer_class(data=request.data)
        
        if not serializer.is_valid():
            return create_response(
                result=f'{[serializer.errors[error][0] for error in serializer.errors][0]}', 
                status=False, 
                code=400, 
                message=message.INVALID_DATA
            )

        email = request.data.get('email')
        user = User.objects.get(email=email)
        user.is_locked = False

        user.save()
        token = get_tokens_for_user(user=user) 
        update_last_login(None, user)
        return create_response(
            result=user.get_User_data(fields=["user_uid", "email", "first_name", "username","last_name", "full_name","oragnization_id", "is_verified", "is_active"]), 
            status=True, 
            code=200, 
            message="Successfully logged in",
            extra={'refresh': token["refresh"],'access':token["access"]}
        )


class UserLogoutView(APIView):
    """ This view is used for logout

    """
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return create_response(
            result="Logged Out Sucessfully", 
            status=True, 
            code=205, 
            message="Logged Out Sucessfully",
            extra={}
        )
        except Exception as e:
            return create_response(
            result=str(e), 
            status=False, 
            code=400, 
            message="Facing issue while logging out.",
            
        )

class UserListView(ListAPIView):
    """ This view is used for edit user data and view user data

    """
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = UserListSerializer

    def get_queryset(self):
        user = self.request.user
        organization_id = user.oragnization_id.oragnization_id
        queryset = self.queryset.order_by('created_at').filter(oragnization_id=organization_id,is_deleted=False)
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        print(queryset)
        serializer = self.serializer_class(queryset,many=True).data

        return create_response(result={'user':serializer},status=True,code=200,message="All users")
        # serializer = self.serializer_class(user, many=False)

class ViewEditUserView(APIView):
    """ This view is used for edit user data and view user data

    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ViewEditUserSerailizer

    def get(self, request, *args, **kwargs):
        try:
            user_id = kwargs.get('user_id')
            user = User.objects.get(user_uid=user_id)
        except Exception as e:
            return create_response(
            result=str(e), 
            status=False, 
            code=400, 
            message="Can't Fetch the User, Please check User ID")
        self.check_object_permissions(request, user)
        # serializer = self.serializer_class(user, many=False)

        return create_response(
            result=user.get_User_data(fields=["all"]),
            status=True, 
            code=200,
            message="User Details"
        )

    def put(self, request, *args, **kwargs):
        """ This method is update the user data

        Kwargs:
            user_id: User's Id
        """
        try:
            user_id = kwargs.get('user_id')
            try:
                user = User.objects.get(user_uid=user_id)
            except Exception as e:
                return create_response(
                    result=str(e), 
                    status=False, 
                    code=400, 
                    message="Can't Fetch the User, Please check User ID")

            self.check_object_permissions(request, user)
            serializer = self.serializer_class(user, data=request.data)
            
            if not serializer.is_valid():
                return create_response(
                    result=serializer.errors, 
                    status=False, 
                    code=400, 
                    message=message.INVALID_DATA
                )
            else:
                data = serializer.save()
                data.full_name = f'{data.first_name} {data.last_name}'
                data.save()
                # To update Profile picture
                if data.profile_picture:
                    img_url = settings.WEBSITE_URL +   f'{data.profile_picture.url}'
                    img_pil = Image.open(requests.get(img_url, stream=True).raw)
                    buffer = io.BytesIO()

                    img_pil.save(fp=buffer, format='PNG')
                    buff_val = buffer.getvalue()
                    buff_val = ContentFile(buff_val)
                    img_name = os.path.basename(data.profile_picture.url).split(".")[0] + '.png'
                    thumb_file = InMemoryUploadedFile(buff_val,'ImageField', img_name, 'image/png', sys.getsizeof(buff_val), None)

                    data.resized_profile_pic = thumb_file
                else:
                    data.resized_profile_pic = ''
                
                data.save()
                result_data = {
                    'email_id': user.email,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'username': user.username,
                    'profile_picture': user.profile_picture.url if user.profile_picture else None,
                    'resized_profile_pic': user.resized_profile_pic.url if user.resized_profile_pic else None,
                    "phone_number" : user.phone_number.national_number if user.phone_number else None,
                    'region' : user.region if user.region else "",
                }

                if user.phone_number is not None:
                    result_data['phone_number'] = str(user.phone_number)

                

            return create_response(
                result=result_data, 
                status=True, 
                code=200,
                message="Updated successfully!"
            )
        except Exception as e:
            return create_response(
                result=str(e), 
                status=False, 
                code=400,
                message="Issue While updating the Profile data"
            )


class ChangePasswordView(APIView):
    """ This view is used to change password

    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer


    def put(self,request,format=None):
        user = request.user
        context = {'user':user}

        serializer = self.serializer_class(data=request.data,context=context)
        if not serializer.is_valid():
            return create_response(
                result=f'{[serializer.errors[error][0] for error in serializer.errors][0]}',
                status=False,
                code=400,
                message=message.INVALID_DATA
            )

        new_password = request.data.get('new_password')
        
        user.set_password(new_password)
        user.save()

        return create_response(
            result=user.get_User_data(fields=["email"]),
            status=True,
            code=200,
            message="Password Changed Successfully."
        )


class ForgotPasswordSendEmailView(APIView):
    """ This view is used to send a link for reset password.

    """
    serializer_class = ForgotPasswordSendEmailViewSerializer
    

    def post(self, request, format=None):
        """ This method is used to send a link for reset passwrod
            when user fogot their password.

        """
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return create_response(
                result=f'{[serializer.errors[error][0] for error in serializer.errors][0]}', 
                status=False, 
                code=400, 
                message=message.INVALID_DATA
            )

        email = request.data.get('email')

        user = User.objects.get(email=email)
        uidb64 = urlsafe_base64_encode(smart_bytes(user.user_uid))
        token = PasswordResetTokenGenerator().make_token(user)

        template_prefix = 'forgot_password'
        subject = 'Reset Password'
        reset_url = settings.MAIL_URL+f"/reset-password/{user.user_uid}/{uidb64}/{token}/"
        template_ctxt = {
            'email': email,
            'first_name': user.first_name,
            'token': token,
            'user_uid': user.user_uid,
            'uidb64' : uidb64,
            'mail_url' : settings.MAIL_URL,
            'reset_url': reset_url
        }

        # send_multi_format_email(template_prefix, template_ctxt, email, subject)
        email_thread = threading.Thread(target=send_multi_format_email,args=[template_prefix, template_ctxt, email, subject],daemon=True)
        email_thread.start()

        return create_response(
            result=user.get_User_data(fields=["email"]), 
            status=True, 
            code=200,
            message="Check your inbox for the reset password link"
        )


class ForgotPasswordVerifyView(APIView):
    """ This view is used to verify the user and update password

    """
    serializer_class = ForgotPasswordVerifyViewSerializer

    def post(self, request, format=None, *args, **kwargs):
        """ This method is used to reset the forgot password

        Kwargs:
            user_id: User's Id
            uid64: Encreypted User ID
            code: Generated Code
            password : New password
        """
        confirmation_token = kwargs.get('token')
        uidb64 = kwargs.get('uidb64')
        new_password = request.data.get('new_password')

        try:
            user_uid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(user_uid=user_uid)
        except Exception as e:
            traceback.print_exc()
            return create_response(
                result=str(e), 
                status=False, 
                code=400, 
                message="Please check Email Adsress, facing issue while collecting the data"
            )
        context = {
            'user': user
        }
        serializer = self.serializer_class(data=request.data, context=context)

        if not serializer.is_valid():
            return create_response(
                result=f'{[serializer.errors[error][0] for error in serializer.errors][0]}', 
                status=False, 
                code=400, 
                message=message.INVALID_DATA
            )

        if not PasswordResetTokenGenerator().check_token(user, confirmation_token):
                raise AuthenticationFailed('The reset link is invalid', 401)
            
        user.set_password(new_password)
        user.is_verified = True
        user.is_active = True
        user.is_locked = False

        user.save()

        return create_response(
                result=user.get_User_data(fields=["email"]), 
                status=True, 
                code=200, 
                message="Password Successfully Updated."
            )

class DeleteExpiredTokenView(APIView):
    """ This view is used to Delete token 

    """
    def get(self, request, *args, **kwargs):
        try:
            BlacklistedToken.objects.filter(token__expires_at__lt=datetime.now()).delete()
            OutstandingToken.objects.filter(expires_at__lt=datetime.now()).delete()


            return create_response(
                    result="Token Deleted Successfully", 
                    status=True, 
                    code=200, 
                    message="Token Deleted Successfully"
                )
        except Exception as e:
            return create_response(
                    result=str(e),
                    status=False, 
                    code=400, 
                    message=str(e)
                )


            
    