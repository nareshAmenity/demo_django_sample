## GENERAL 
INVALID_DATA = "Invalid Data"
QUERY_PARAMS_NOT_FOUND = "Please check Query Params."
PAGE_NOT_FOUND = "Please check Data is not availabel for given params, Page not Found"
UNAUTHORIZE_ACCESS = "You are not authorize to access this page"
MISMATCH_DATA_FAIL = "Data Doesn't Exists for this query."
WRONG_DATA = "Please check Data, table not updated"
DATA_EXIST = "Data Already exists for this organization"



# Account(user) Serializer Message
NECESSARY_EMAIL_ADDRESS = "Email Adderess is Required"
NECESSARY_PASSWORD = "'Password is required'"




#PUT SUCCESS
OVERALL_ASSEST_UPDATE_SUCCESS = "Overall Assests Data Updated Succssfully"
OPEN_PORT_UPDATE_SUCCESS = "Open Port Data Updated Succssfully"
APPLICATION_UPDATE_SUCCESS = "Application Data Updated Succssfully"
SSL_UPDATE_SUCCESS = "Ssl Data Updated Succssfully"
DARKWEB_UPDATE_SUCCESS = "Darkweb Data Updated Succssfully"


ASSEST_COUNT_SUCCESS = "Fetched Assest Count Successfully"
LOOKLIKE_COUNT_DATA_SUCCESS = "Fetched Looklike table count Successfully"
SSL_COUNT_DATA_SUCCESS = "Fetched Ssl table count Successfully"



BRANDMONITORING_COUNT_DATA_SUCCESS = "Fetched All table count Successfully"

TELEGRAM_UPDATE_SUCCESS = "Telegram Web Data Updated Succssfully"
TWITTER_UPDATE_SUCCESS = "Twitter Web Data Updated Succssfully"
YOUTUBE_UPDATE_SUCCESS = "Youtube Data Updated Succssfully"

## Email Security
# Table

EMAIL_SECURITY_DATA_SUCCESS = "Email Security Data Fetched Successfully"
 
# Update email security
SPF_UPDATE_SUCCESS = "Spf Data Updated Successfully."
DMARC_UPDATE_SUCCESS = "Dmarc Data Updated Successfully."
DKIM_UPDATE_SUCCESS = "Dkim Data Updated Successfully."
BIMI_UPDATE_SUCCESS = "Dmarc Data Updated Successfully."
