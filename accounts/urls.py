from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from accounts.views import UserLoginView,UserLogoutView,ViewEditUserView,ChangePasswordView,\
                            ForgotPasswordVerifyView,DeleteExpiredTokenView,UserListView,ForgotPasswordSendEmailView,error404
handler404 = error404
Prefix_API = 'api/v1'
urlpatterns = [

         
    path(f'{Prefix_API}/login/',UserLoginView.as_view(),name="Login"), 
    path(f'{Prefix_API}/logout/',UserLogoutView.as_view(),name="Login"),

    
    path(f'{Prefix_API}/view-user-details/<slug:user_id>/', ViewEditUserView.as_view(), name='view_user_details'),
    path(f'{Prefix_API}/edit-user-details/<slug:user_id>/', ViewEditUserView.as_view(), name='edit_user_details'),
    path(f'{Prefix_API}/change-password/', ChangePasswordView.as_view(), name='change_password'),
    path(f'{Prefix_API}/user-list/', UserListView.as_view(), name='user_list'),

    
    path(f'{Prefix_API}/reset-password/', ForgotPasswordSendEmailView.as_view(), name='reset_password'),
    path(f'{Prefix_API}/reset-password/verify/<str:user_uid>/<str:uidb64>/<str:token>/', ForgotPasswordVerifyView.as_view(), name='reset_password_verify'),

    
    path(f'{Prefix_API}/delete-expired-token/', DeleteExpiredTokenView.as_view(), name='delete_expired_token'),
    
]