import requests
from ASM_Backend import constant,settings
import os
import psycopg2
from dotenv import load_dotenv
import os
import datetime
import traceback
DB_NAME = os.environ.get('DB_NAME')
DB_USER = os.environ.get('DB_USER')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_HOST = os.environ.get('DB_HOST')
PORT = os.environ.get('PORT')

class CreateDbConnection:
	def __init__(self) -> None:
		
		self.conn = psycopg2.connect(database=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port='5432')
		self.cursor = self.conn.cursor()
		self.cursor.execute("select version()")
		data = self.cursor.fetchone()
		print(f"#########, Connection established to: {data} #########")
		self.fetchAllTables()

	def fetchAllTables(self):
		"""
		This will fetch all the database tables.
		"""
		self.tablelist = []
		self.cursor.execute("""SELECT table_name FROM information_schema.tables
			WHERE table_schema = 'public'""")
		# print(self.cursor.fetchall())
		for table in self.cursor.fetchall():
			self.tablelist.append(table[0])

	def printAllTables(self):
		"""
		Print the name of the tables
		"""
		print("#################################################")
		for table in self.tablelist:
			print(table)
		print("#################################################")
		

	def getTableColumnNames(self,tablename:str,printcolomns=True) -> list:
		"""
		Get names of coloum in particular table
		* tablename(str) : Tablename of which colomn will be printed
		"""
		if tablename not in self.tablelist:
			print(f"{tablename} Doesn't Exist Please check Table Name in Below List -> ")
			self.printAllTables()
			return
		self.cursor.execute(f"Select * FROM {tablename} LIMIT 0")
		colnames = [desc[0] for desc in self.cursor.description]
		if printcolomns:
			print(f"COLOMN NAMES  : {colnames}")
		return colnames

	def storeData(self,tablename: str,data_list: list,id: str,ignore_list=[],mutilple_data=False) -> None:
		"""
		Store the data or update the data:
		Store if data doesn't exist other wise updated the data

		* tablename(str) : Name of table to update or insert the data
		* data_list(list) : list of dictionaries with column of table
		* id(str) : common which decides insert or update call
		"""
		# INSERT INTO incident_priority (created_at, updated_at, is_deleted, priority_name) VALUES (%s, %s, %s, %s) ON CONFLICT (priority_name) DO UPDATE SET (updated_at, is_deleted) =  ROW(EXCLUDED.updated_at, EXCLUDED.is_deleted).. 
		try:
			data = []
			query = ""
			for data_dict in data_list:
				if not query:
					columns = ', '.join('{0}'.format(k) for k in data_dict)
					place_holders = ', '.join('%s'.format(k) for k in data_dict)
					query = "INSERT INTO {0} ({1}) VALUES ({2})".format(tablename, columns, place_holders)
					if mutilple_data:
						columns_updated = ', '.join('{0}'.format(k) for k in data_dict if (k not in id and k!='created_at') and  not (k in ignore_list))
						columns_updated_exlcuded = ', '.join('{0}'.format("EXCLUDED."+k) for k in data_dict if (k not in id and k!='created_at') and not (k in ignore_list))
						ids = ', '.join('{0}'.format(k) for k in id)
						query = "{0} ON CONFLICT ({2}) DO UPDATE SET ({3}) =  ROW({1})".format(query, columns_updated_exlcuded,ids,columns_updated)
						
					else:
						columns_updated = ', '.join('{0}'.format(k) for k in data_dict if (k!=id and k!='created_at') and  not (k in ignore_list))
						columns_updated_exlcuded = ', '.join('{0}'.format("EXCLUDED."+k) for k in data_dict if (k!=id and k!='created_at') and not (k in ignore_list))
						query = "{0} ON CONFLICT ({2}) DO UPDATE SET ({3}) =  ROW({1})".format(query, columns_updated_exlcuded,id,columns_updated)
					# print("QUERY : ",query)
				v = list(data_dict.values())
				data.append(v)
			# logger.info(f"DB Query.. => {query}.. ", extra={'file_id': log_file_info_level})
			self.cursor.executemany(query,data)
			self.conn.commit()

		except Exception as e:
			traceback.print_exc()
			print("######################## : ",e)
			# logger.error(f"Error=> {e}", extra={'file_id': log_file_error})

	
	def InsertData(self,tablename: str,data_list: list) -> None:
		"""
		Store the data or update the data:
		Store if data doesn't exist other wise updated the data

		* tablename(str) : Name of table to update or insert the data
		* data_list(list) : list of dictionaries with column of table
		* id(str) : common which decides insert or update call
		"""
		try:
			data = []
			query = ""
			for data_dict in data_list:
				if not query:
					columns = ', '.join('{0}'.format(k) for k in data_dict)
					duplicates = ', '.join('{0}=VALUES({0})'.format(k) for k in data_dict)
					place_holders = ', '.join('%s'.format(k) for k in data_dict)
					query = "INSERT INTO {0} ({1}) VALUES ({2})".format(tablename, columns, place_holders)
					
					# columns_updated = ', '.join('{0}'.format(k) for k in data_dict if (k!=id and k!='created_at') and  not (k in ignore_list))
					# columns_updated_exlcuded = ', '.join('{0}'.format("EXCLUDED."+k) for k in data_dict if (k!=id and k!='created_at') and not (k in ignore_list))
					# query = "{0} ON CONFLICT ({2}) DO UPDATE SET ({3}) =  ROW({1})".format(query, columns_updated_exlcuded,id,columns_updated)
					print("QUERY : ",query)
				v = list(data_dict.values())
				data.append(v)
			# logger.info(f"DB Query.. => {query}.. ", extra={'file_id': log_file_info_level})
			self.cursor.executemany(query,data)
			self.conn.commit()

		except Exception as e:
			traceback.print_exc()
			print("######################## : ",e)
			# logger.error(f"Error=> {e}", extra={'file_id': log_file_error})

			


	def getRowId(self,tablename: str,key: str,value: str,fetchData='*',wholedata=False):
		"""
		based on particular value fetch row index from table

		* tablename(str) : Name of the table where we are going to find the raw
		* key(str) : key name which we are going to compare
		* value(str) : value whose index we want to find
		"""
		query = f"SELECT {fetchData} FROM {tablename} WHERE {key} ='{value}'"
		self.cursor.execute(query)
		results = self.cursor.fetchall()
		if len(results) == 1 and (not wholedata):
			return results[0][0],False
		else:
			return results,True


	def closeConnection(self) -> None:
		"""
		Close connection of database after task is done
		"""
		self.conn.close() 
